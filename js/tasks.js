// Write Classes
export default class Task {
	constructor(name, priority){
		this.name = name;
		this.priority = priority
	}
	// create a method
	show() {
		console.log(`${this.name} have a priority of ${this.priority}`);
	}
}
