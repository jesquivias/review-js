import Task from './tasks.js';
import PendingPurchases from './purchases.js'

const task1  = new Task('Learn JSX', 'Urgency');

console.log(task1);

task1.show();

const purchase1 = new PendingPurchases('Pizza', 'Urgency', 2);

console.log(purchase1);

purchase1.show();
purchase1.hello();
