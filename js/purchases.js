import Task from './tasks.js';

export default class PendingPurchases extends Task {
	constructor(name, priority, quantity) {
		super(name, priority);
		this.quantity = quantity;
	}
	show() {
		super.show();
		console.log(`and the quantity of ${this.quantity}`);
	}
	hello() {
		console.log('hello');
	}
}
